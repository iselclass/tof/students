#!/usr/bin/env python
# coding: utf-8

# ## Exercise template for student activity within TOF 
# To be presented: 19th of May 2020

# 
# # Polarization state engineering - classical vs quantum 
# References to be used: 
# 
# - Resources on the TOF moodle page (polarization paradox, dirac notation, qm concept)
# 
# - Mark Becks book "Quantum Mechanics - Theory and experiment" (two state description, jones calculus)
# 
# - Dawes QMlabs page and examples therein (jupyter notebook)
# 
# - Examples from the qutip project page (for working with the bloch sphere )

# ## Problems to be addressed:
# 
# 1. When considering light polarization experiments: what is the difference (technically and concept) between classical and quantum description of probabilities ?
# 
# 2. For Dirac's polarization paradoxon, calculate the classical and quantum probabilities, for a particular case (A, B, C, D, as below) implementation.
# 
# 3. Insert in the middle a HWP retarder, and create a graphic for a 360 deg rotation
# 
# 4. Insert in the middle a QWP retarder, and create a graphic for a 360 deg rotation
# 
# 4. Create the visualization of the Bloch sphere of the two situations.
# 
# Case A: Emitter H, Detector A 
# 
# Case B: Emitter V, Detector D
# 
# Case C:Emitter D, Detector H
# 
# Case D:Emitter A, Detector V
# 
# 
# 

# In[1]:


from numpy import sqrt
from qutip import *


# Initial polarization state definitions from Dawes:

# In[2]:


H = Qobj([[1],[0]])
V = Qobj([[0],[1]])
D = Qobj([[1/sqrt(2)],[1/sqrt(2)]])
A = Qobj([[1/sqrt(2)],[-1/sqrt(2)]])
R = Qobj([[1/sqrt(2)],[-1j/sqrt(2)]])
L = Qobj([[1/sqrt(2)],[1j/sqrt(2)]])


# In[ ]:


bracker = pow(np.absolute((V.dag()* A)1,2))


# Device / Operator definitions from Dawes:
# 
# HWP - Half-wave plate axis at $\theta$ to the horizontal
# 
# LP - Linear polarizer, axis at $\theta$
# 
# QWP - Quarter-wave plate, axis at $\theta$

# In[3]:


def HWP(theta):
    return Qobj([[cos(2*theta),sin(2*theta)],[sin(2*theta),-cos(2*theta)]]).tidyup()


# In[1]:


def LP(theta):
    return Qobj([[cos(theta)**2,cos(theta)*sin(theta)],[sin(theta)*cos(theta),sin(theta)**2]]).tidyup()


# In[2]:


def QWP(theta):
    return Qobj([[cos(theta)**2 + 1j*sin(theta)**2,
                 (1-1j)*sin(theta)*cos(theta)],
                [(1-1j)*sin(theta)*cos(theta),
                 sin(theta)**2 + 1j*cos(theta)**2]]).tidyup()


# In[3]:


HWP(math.pi/4) * HWP(math.pi/4)


# In[1]:


Qobj data = 
([1, 0]
 [[1/sqrt(2)],[-1/sqrt(2)]])


# In[ ]:


Ang= [0,math.pi/24, math.pi/16, math.pi/12, math.pi/6, 5*math.pi/24, math.pi/8]


# In[ ]:


r=[]
for *in Ang:
    
    r =append(pow(np.absolute((H.dag() * HWP(x) * 2)),2))


# In[ ]:


print(*r)
t = []
for 1 in.range(l/in(Ang))

